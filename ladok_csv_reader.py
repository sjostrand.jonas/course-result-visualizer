import datetime
import ast
import pandas as pd
import matplotlib
import matplotlib.backends.backend_tkagg
import matplotlib.pyplot as plt
import matplotlib.ticker as mtick
from typing import List, Dict
from tkinter import Tk, filedialog, Button, Entry, Label, END, StringVar, X, LEFT, RIGHT, Spinbox, IntVar, Listbox, Toplevel, SOLID
from tkinter.ttk import Frame


class ToolTip(object):

    def __init__(self, widget):
        self.widget = widget
        self.tipwindow = None
        self.id = None
        self.x = self.y = 0

    def showtip(self, text):
        "Display text in tooltip window"
        self.text = text
        if self.tipwindow or not self.text:
            return
        x, y, cx, cy = self.widget.bbox("insert")
        x = x + self.widget.winfo_rootx() + 57
        y = y + cy + self.widget.winfo_rooty() +27
        self.tipwindow = tw = Toplevel(self.widget)
        tw.wm_overrideredirect(1)
        tw.wm_geometry("+%d+%d" % (x, y))
        label = Label(tw, text=self.text, justify=LEFT,
                      background="#ffffe0", relief=SOLID, borderwidth=1,
                      font=("tahoma", "8", "normal"))
        label.pack(ipadx=1)

    def hidetip(self):
        tw = self.tipwindow
        self.tipwindow = None
        if tw:
            tw.destroy()

def create_tool_tip(widget, text):
    toolTip = ToolTip(widget)
    def enter(event):
        toolTip.showtip(text)
    def leave(event):
        toolTip.hidetip()
    widget.bind('<Enter>', enter)
    widget.bind('<Leave>', leave)


class FileEntry(Frame):
    def __init__(self, parent, label, var):
        super().__init__(parent)
        self.pack(fill=X, expand=True)
        Label(self, text=label).pack(side=LEFT, padx=5, pady=5)
        Button(self, text="Browse", command=self.onOpen).pack(side=RIGHT, padx=5, pady=5)
        self.entry = Entry(self, textvariable=var)
        self.entry.pack(side=RIGHT, fill=X, padx=5, expand=True)

    def onOpen(self):
        ftypes = [('Csv files', '*.csv'), ('All files', '*')]
        dlg = filedialog.Open(self, filetypes=ftypes)
        filename = dlg.show()
        self.entry.delete(0, END)
        self.entry.insert(0, filename)


class LabelledEntry(Frame):
    def __init__(self, parent, label, var):
        super().__init__(parent)
        self.pack(fill=X, expand=True)
        Label(self, text=label).pack(side=LEFT, padx=5, pady=5)
        Entry(self, textvariable=var).pack(side=RIGHT, fill=X, padx=5, expand=True)


class LabelledSpinbox(Frame):
    def __init__(self, parent, label, var):
        super().__init__(parent)
        self.pack(fill=X, expand=True)
        Label(self, text=label).pack(side=LEFT, padx=5, pady=5)
        Spinbox(self, from_=-1000, to=1000, textvariable=var).pack(side=RIGHT, fill=X, padx=5, expand=True)


class LabelledListbox(Listbox):
    def __init__(self, parent, label, options):
        h_frame = Frame(parent)
        h_frame.pack(fill=X, expand=True)
        Label(h_frame, text=label).pack(side=LEFT, padx=5, pady=5)
        Listbox.__init__(self, h_frame, selectmode='multiple')
        for option in options:
            self.insert(END, option)
        self.pack(side=RIGHT, fill=X, padx=5, expand=True)


class MyFrame(Frame):
    def __init__(self, root):
        def reset(*args):
            self.reset()

        super().__init__(root)
        self.master.title("Course result visualizer")
        self.pack(fill=X, expand=True)

        self.participation_entry_var = StringVar()
        self.participation_entry_var.trace_add('write', reset)
        participation_file_widget = FileEntry(self, label="Participation file:", var=self.participation_entry_var)
        create_tool_tip(participation_file_widget, 'CSV-file exported from Ladok (Utdata: Deltagande kurs, alla sammanfattade tillstånd, alla datum)')

        self.result_entry_var = StringVar()
        self.result_entry_var.trace_add('write', reset)
        result_file_widget = FileEntry(self, label="Result file:", var=self.result_entry_var)
        create_tool_tip(result_file_widget, 'CSV-file exported from Ladok (Utdata: Resultat, visa moduler, inkludera avbrott, alla datum)')

        self.min_num_registered_var = IntVar(value=10)
        self.min_num_registered_var.trace_add('write', reset)
        min_num_registered_spinbox = LabelledSpinbox(self, label="Minimum number of registered students:", var=self.min_num_registered_var)
        create_tool_tip(min_num_registered_spinbox, 'The smallest number of registered students for a course offering and programme to be included')

        self.last_chance_offset_var = IntVar(value=30)
        self.last_chance_offset_var.trace_add('write', reset)
        last_chance_offset_widget = LabelledSpinbox(self, label="Last-chance offset in days:", var=self.last_chance_offset_var)
        create_tool_tip(last_chance_offset_widget, 'The number of days after the end date included in a course offering. If you want to include the first re-exam, maybe 60 is a good number.')

        self.start_date_to_last_chance_date_var = StringVar(value='{}')
        self.start_date_to_last_chance_date_var.trace_add('write', reset)
        start_date_to_last_chance_widget = LabelledEntry(self, label="Start date to last-chance date:", var=self.start_date_to_last_chance_date_var)
        create_tool_tip(start_date_to_last_chance_widget, 'Special last-chance dates for individual course offerings. For instance, the course offering of MAA149 starting at 2020-01-20 didn\'t have its final exam until June 1 because of the pandemic, so we should provide {\'2020-01-20\': \'2020-06-01\'}.')

        Button(self, text="Compute statistics", command=self.compute_statistics).pack(padx=5, pady=5)

        self.program_listbox = LabelledListbox(self, label='Programmes:', options=[])
        self.visualize_statistics_button = Button(self, text="Visualize statistics", command=self.visualize_statistics)
        self.visualize_statistics_button.pack(side=RIGHT, padx=5, pady=5)

        self.reset()

    def reset(self):
        self.stat_df = None
        self.figure_text = None
        self.programs = None
        if hasattr(self, 'program_listbox'):
            self.program_listbox['state'] = 'disabled'
        if hasattr(self, 'visualize_statistics_button'):
            self.visualize_statistics_button['state'] = 'disabled'

    def compute_statistics(self):
        start_date_to_last_chance_date = ast.literal_eval(self.start_date_to_last_chance_date_var.get())
        self.stat_df, self.figure_text, self.programs =\
            compute_statistics(path_to_participation=self.participation_entry_var.get(),
                               path_to_results=self.result_entry_var.get(),
                               min_num_registered=int(self.min_num_registered_var.get()),
                               last_chance_offset_in_days=int(self.last_chance_offset_var.get()),
                               start_date_to_last_chance_date=start_date_to_last_chance_date)
        self.program_listbox['state'] = 'normal'
        self.program_listbox.delete(0, END)
        for program in self.programs:
            self.program_listbox.insert(END, program)
        self.program_listbox.select_set(0, END)
        self.visualize_statistics_button['state'] = 'normal'

    def visualize_statistics(self):
        selected_indices = list(self.program_listbox.curselection())
        selected_programs = [self.programs[i] for i in selected_indices]
        visualize_statistics(stat_df=self.stat_df,
                             figure_text=self.figure_text,
                             programs_to_plot=selected_programs)


def string_to_date(series):
    return pd.to_datetime(series, format='%Y-%m-%d', errors='coerce')


def format_string_list(strings: List[str]) -> str:
    if len(strings) == 0:
        return ""
    if len(strings) == 1:
        return strings[0]
    result = strings[0]
    for string in strings[1:-1]:
        result += ', ' + string
    result += ' and ' + strings[-1]
    return result


PERSONNUMMER = "Personnummer (Student)"
EFTERNAMN = "Efternamn (Student)"
FORNAMN = "Förnamn (Student)"
SAMMANFATTAT_TILLSTAND = "Tillstånd (Sammanfattat tillstånd)"
KURSKOD = "Kod (Kurs)"
KURSTILLFALLESKOD = "Kod (Kurstillfälle)"
STARTDATUM = "Startdatum (Kurstillfälle)"
SLUTDATUM = "Slutdatum (Kurstillfälle)"
TILLSTAND = "Tillstånd (Studieperiod)"
PROGRAM = "Kurspaketering kod (Läses inom)"

NUM_REGISTERED = "Antal registrerade"
NUM_WHO_TRIED = "number_who_tried"
NUM_WHO_PASSED_AT_LEAST_ONE_MOMENT = "number_who_passed_at_least_one_moment"
NUM_WHO_PASSED_THE_COURSE = "number_who_passed_the_course"
FRACTION_WHO_TRIED = "fraction_who_tried"
FRACTION_WHO_PASSED_AT_LEAST_ONE_MOMENT = "fraction_who_passed_at_least_one_moment"
FRACTION_WHO_PASSED_THE_COURSE = "fraction_who_passed_the_course"
PASSED_OVER_TRIED = "passed_over_tried"


def compute_statistics(path_to_participation: str,
                       path_to_results: str,
                       min_num_registered: int = 10,
                       last_chance_offset_in_days: int = 30,
                       start_date_to_last_chance_date: Dict[str, str] = {}):
    """Compute time-development statistics of the results in a single course from exported Ladok data.

    :param path_to_participation:
        File exported from Ladok (Utdata: Deltagande kurs, alla sammanfattade tillstånd, alla datum)
    :param path_to_results:
        File exported from Ladok (Utdata: Resultat, visa moduler, inkludera avbrott, alla datum)
    :param min_num_registered:
        The smallest number of registered students for a course offering to be included
    :param last_chance_offset_in_days:
        The number of days after the end date included in a course offering. If you want to include the first re-exam,
        maybe 60 is a good number.
    :param start_date_to_last_chance_date:
        Special last-chance dates for individual course offerings. For instance, the course offering of MAA149 starting
        at 2020-01-20 didn't have its final exam until June 1 because of the pandemic, so we should provide
        {'2020-01-20': '2020-06-01'}.
    :return: A triple `(stat_df, figure_text, programs)`, where `stat_df` holds the statistics and `programs` is a list
        of all educational programs in `stat_df`.
    """
    all_columns = [PERSONNUMMER, KURSKOD, STARTDATUM, SLUTDATUM, TILLSTAND, PROGRAM]

    MODUL = "Kod (Modul)"
    BETYG = "Betyg (Resultat)"
    EXAMENSDATUM = "Ex. datum (Resultat)"

    def read_csv(path, usecols):
        with open(path, 'r') as f:
            header = 0
            while not f.readline().isspace():
                header += 1
        return pd.read_csv(path, delimiter=';', header=header, usecols=usecols)

    df = read_csv(path_to_participation, usecols=all_columns)
    course_code = df[KURSKOD][0]
    assert (df[KURSKOD] == course_code).all()
    df = df.drop(KURSKOD, axis=1)
    df = df.dropna()

    res_df = read_csv(path_to_results, usecols=[PERSONNUMMER, MODUL, BETYG, EXAMENSDATUM])
    THE_COMPLETE_COURSE = "hela_kursen"
    res_df = res_df.fillna({MODUL: THE_COMPLETE_COURSE})
    res_df = res_df.dropna()
    res_df = res_df.set_index(PERSONNUMMER)

    #df = df[(df[TILLSTAND] == "Registrerad") | (df[TILLSTAND] == "Omregistrerad")]
    df = df[df[TILLSTAND] == "Registrerad"]
    assert df[PERSONNUMMER].is_unique

    for col in [STARTDATUM, SLUTDATUM]:
        df[col] = string_to_date(df[col])
    LAST_CHANCE_DATE = "last_chance_for_examination"
    df[LAST_CHANCE_DATE] = df[SLUTDATUM] + pd.DateOffset(days=last_chance_offset_in_days)
    for start_date_str, last_chance_date_str in start_date_to_last_chance_date.items():
        start_date, last_chance_date = (datetime.datetime.fromisoformat(s) for s in (start_date_str,
                                                                                     last_chance_date_str))
        assert (df[STARTDATUM] == start_date).any()
        df.loc[df[STARTDATUM] == start_date, LAST_CHANCE_DATE] = last_chance_date

    comb_df = df.join(res_df, on=PERSONNUMMER)
    TIMELY = "is_within_time"
    comb_df[TIMELY] = (comb_df[STARTDATUM] <= comb_df[EXAMENSDATUM]) &\
                      (comb_df[EXAMENSDATUM] <= comb_df[LAST_CHANCE_DATE])
    PASSED_WITHIN_TIME = "is_passed_within_time"
    PASSED_COURSE_WITHIN_TIME = "passed_course_within_time"
    comb_df[PASSED_WITHIN_TIME] = comb_df[BETYG].isin(["G", "3", "4", "5"]) & comb_df[TIMELY]
    comb_df[PASSED_COURSE_WITHIN_TIME] = comb_df[PASSED_WITHIN_TIME] & (comb_df[MODUL] == THE_COMPLETE_COURSE)

    TRIED_AT_LEAST_ONE_MOMENT_WITHIN_TIME = "tried_at_least_one_moment_within_time"
    PASSED_AT_LEAST_ONE_MOMENT_WITHIN_TIME = "passed_at_least_one_moment_within_time"
    agg_df = comb_df.groupby(PERSONNUMMER)\
           .agg(**{TRIED_AT_LEAST_ONE_MOMENT_WITHIN_TIME: (TIMELY, "any"),
                   PASSED_AT_LEAST_ONE_MOMENT_WITHIN_TIME: (PASSED_WITHIN_TIME, "any"),
                   PASSED_COURSE_WITHIN_TIME: (PASSED_COURSE_WITHIN_TIME, "any")})

    df = df.join(agg_df, on=PERSONNUMMER)

    stat_df = df.groupby([STARTDATUM, PROGRAM])\
                   .agg(**{NUM_REGISTERED: (PERSONNUMMER, 'count'),
                           NUM_WHO_TRIED: (TRIED_AT_LEAST_ONE_MOMENT_WITHIN_TIME, "sum"),
                           FRACTION_WHO_TRIED: (TRIED_AT_LEAST_ONE_MOMENT_WITHIN_TIME, "mean"),
                           NUM_WHO_PASSED_AT_LEAST_ONE_MOMENT: (PASSED_AT_LEAST_ONE_MOMENT_WITHIN_TIME, "sum"),
                           FRACTION_WHO_PASSED_AT_LEAST_ONE_MOMENT: (PASSED_AT_LEAST_ONE_MOMENT_WITHIN_TIME, "mean"),
                           NUM_WHO_PASSED_THE_COURSE: (PASSED_COURSE_WITHIN_TIME, "sum"),
                           FRACTION_WHO_PASSED_THE_COURSE: (PASSED_COURSE_WITHIN_TIME, "mean")})\
                   .reset_index()

    stat_df[PASSED_OVER_TRIED] = stat_df[NUM_WHO_PASSED_THE_COURSE] / stat_df[NUM_WHO_TRIED]

    stat_df = stat_df[stat_df[NUM_REGISTERED] >= min_num_registered]

    maybe_text = ""
    if len(start_date_to_last_chance_date) == 1:
        start_date_str, last_chance_date_str = list(start_date_to_last_chance_date.items())[0]
        maybe_text = f", except for the offering starting at {start_date_str} which extends to {last_chance_date_str}"
    elif len(start_date_to_last_chance_date) > 1:
        start_dates, last_chance_dates = start_date_to_last_chance_date.keys(), start_date_to_last_chance_date.values()
        start_dates_str, last_chance_dates_str = (format_string_list(list(s)) for s in (start_dates, last_chance_dates))
        maybe_text = f", except for the offerings starting at {start_dates_str} which extend to " \
                     f"{last_chance_dates_str}, respectively"

    figure_text = f"The development of the course {course_code} over time with respect to three parameters sliced on " \
                  f"educational programme. The x-axis marks the start date of the course offering. Only first-time " \
                  f"registered students are included. To be regarded as following the course you must have a (not " \
                  f"necessarily passing) result on at least one Ladok module. The examination rate is the fraction " \
                  f"that passed the complete course among the students that followed it. Only results up to " \
                  f"{last_chance_offset_in_days} days after the end date of the course offering are " \
                  f"included{maybe_text}."

    programs = list(stat_df[PROGRAM].unique())

    return stat_df, figure_text, programs



def visualize_statistics(stat_df,
                         figure_text: str,
                         programs_to_plot: List[str]):
    """
    Visualize the statistics previously computed.

    :param stat_df:
        The statistics panda frame

    :param figure_text:
        The figure text

    :param programs_to_plot:
        A list of programme abbreviations, like ['GCV02', 'CCV20', 'GCV03', 'RMV20']
    """

    matplotlib.use('TkAgg')
    fig, axes = plt.subplots(3)
    for program in programs_to_plot:
        df = stat_df[stat_df[PROGRAM] == program]
        axes[0].plot(df[STARTDATUM], df[NUM_REGISTERED], "o-", clip_on=False, label=program)
        axes[1].plot(df[STARTDATUM], df[FRACTION_WHO_TRIED], "o-", clip_on=False, label=program)
        axes[2].plot(df[STARTDATUM], df[PASSED_OVER_TRIED], "o-", clip_on=False, label=program)

    for i in (1, 2):
        axes[i].yaxis.set_major_formatter(mtick.PercentFormatter(1.0))

    axes[0].set(ylabel="number of registered", ylim=(0, None))
    axes[1].set(ylabel="fraction following the course", ylim=(0, 1))
    axes[2].set(ylabel="examination rate", ylim=(0, None))

    for i in range(3):
        axes[i].grid(True)

    fig.subplots_adjust(top=0.95, left=0.1, right=0.9, bottom=0.25)
    axes.flatten()[-1].legend(loc='upper center', bbox_to_anchor=(0.5, -0.15), ncol=len(programs_to_plot))

    fig.set_size_inches(9.5, 7.5, forward=True)

    plt.figtext(0.0, 0.05, figure_text, wrap=True, horizontalalignment='left', fontsize=10)
    plt.show()


if __name__ == "__main__":
    root = Tk()
    root.geometry("800x600+300+100")
    app = MyFrame(root)
    root.mainloop()

    #path_to_participation = "C:\\Users\\jsd02\\Documents\\kurser\\MAA141\\deltagande_MAA141.csv"

    # stat_df, figure_text, programs = compute_statistics(
    #     path_to_participation="C:\\Users\\jsd02\\Documents\\kurser\\MAA149\\deltagande_MAA149.csv",
    #     path_to_results="C:\\Users\\jsd02\\Documents\\kurser\\MAA149\\resultat_MAA149.csv",
    #     min_num_registered=10,
    #     last_chance_offset_in_days=30,
    #     start_date_to_last_chance_date={'2020-01-20': '2020-06-01'})
    #
    # visualize_statistics(stat_df,
    #                      figure_text,
    #                      programs_to_plot=["GCV02", "CCV20", "GCV03", "RMV20"])

